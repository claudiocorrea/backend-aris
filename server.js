var httpPort = 80,
    httpsPort = 443,
    http = require('http'),
    https = require('https'),
    httpServer, httpsServer,

    arisConfig = require('./config/aris_config'),

    fs = require('fs'),
    privateKey  = fs.readFileSync('./sslcert/private.key', 'utf8'),
    certificate = fs.readFileSync('./sslcert/certificate.crt', 'utf8'),
    ca = fs.readFileSync('./sslcert/ca_bundle.crt', 'utf8'),
    credentials = { key: privateKey, cert: certificate, ca: ca },
    
	express = require('express'),
    app = express(),
    session = require('express-session'),
    helmet = require('helmet'),
    compression = require('compression'),
    bodyParser = require('body-parser'),
    path = require('path'),

	router = express.Router(),
    routerServices = require('./routes/services');

global.config = arisConfig;

app.use(helmet());
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    res.header('Cache-Control', 'no-cache');

    if ( req.method == 'OPTIONS') { res.sendStatus(200); res.end(); } else { next(); }
})

app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
    secret: 'Simbius*2019',
    resave: true,
    saveUninitialized: true,
    user: null,
    token: null,
    name: 'ARISBackend'
}));

//app.use(express.static('./webapp'));

/*
router.get('/', function (req, res, next) {
    res.sendFile(path.join(__dirname + '/webapp/index.html'));
});
*/

app.use(router);
app.use('/services', routerServices);


httpServer = http.createServer(app).listen(httpPort, function () {
    console.log('-- Servidor HTTP iniciado (port: ' + httpPort + ') - ' + new Date() + '... ')
});

httpsServer = https.createServer(credentials, app).listen(httpsPort, function () {
    console.log('-- Servidor HTTPS iniciado ( port: ' + httpsPort + ') - ' + new Date() + '... ');
});