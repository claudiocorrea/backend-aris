var rp = require('request-promise'),
    options = {};

exports.getToken = function (userData) {
    options = {
        method: 'POST',
        url: global.config.getFullAPI('tokens'),
        qs: {
            tenant: userData.tenant,
            name: userData.name,
            password: userData.password,
            key: global.config.getARISKey(false)
        },
        json: true,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Accept': 'application/json'
        }
    }

    return rp(options);
}