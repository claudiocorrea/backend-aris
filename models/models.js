var rp = require('request-promise'),
    options = {};

    exports.getModelContent = function (params, token) {
        var qs = {
            withcontent: params.withContent,
            language: params.language
        }
    
        if (params.attributes) {
            qs.attributes = params.attributes;
        }
        options = {
            method: 'GET',
            url: global.config.getFullAPI('models') + '/' + params.dbName + '/' + params.modelGUID,
            qs: qs,
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json',
                'Cookie': 'accesstoken=' + token
            },
            json: true
        }
    
        return rp(options);
    }
    
    exports.getModelGraphic = function (params, token) {
        options = {
            method: 'GET',
            url: global.config.getFullAPI('models') + '/' + params.dbName + '/' + params.modelGUID + '/graphic?language=' + params.language,
            headers: {
                // 'Content-Type': 'application/json; charset=UTF-8',
                // 'Accept': 'application/json',
                'Cookie': 'accesstoken=' + token
            },
            json: true
        };
        return rp(options);
    }