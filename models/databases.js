var rp = require('request-promise'),
    options = {};

exports.getDataBases = function (token) {
    options = {
        method: 'GET',
        url: global.config.getFullAPI("databases"),
        headers: {
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept': 'application/json',
            'Cookie': 'accesstoken=' + token
        },
        json: true
    }

    return rp(options);
};

exports.findItemsInDatabase = function (params, token) {
    var qs = {};
    
    Object.keys(params).forEach( function (k) {
        if (k === "filters") {
            Object.keys(params[k]).forEach(function (fk) {
                qs[fk] = params.filters[fk];
            })
        } else qs[k] = params[k]
    } )

    options = {
        method: 'GET',
        url: global.config.getFullAPI("databases") + '/' + params.dbName + '/find',
        qs: qs,
        headers: {
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept': 'application/json',
            'Cookie': 'accesstoken=' + token
        },
        json: true
    }
    console.log("URL: " + options.url, options.qs);
    return rp(options);
}