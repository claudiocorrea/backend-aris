var databasesModel = require('../models/databases'),
    modelsController = require('../controllers/models'),
    tokensController = require('../controllers/tokens');

exports.getDataBases = (req, res) => {
    databasesModel.getDataBases(req.session.token)
        .then((parsedResponse) => {
            res.status(200).setHeader('Content-Type', 'application/json');
            res.send({success: true, databases: parsedResponse.items});
        }).catch((reason) => {
            console.log("- getDataBases ERROR: ", reason);
            var statusCode = 500;
            if (reason.statusCode) statusCode = reason.statusCode;
            res.status(statusCode).setHeader('Content-Type', 'application/json');
            res.send({success: false, response: reason});
        });
};

exports.findItemsInDatabase = (req) => {
    var presetFilters, params = {}, filters = {};    

    if (req.query) {
        Object.keys(req.query).forEach(function (q) {
            filters[q] = req.query[q];
        })
    }
    params = {
        dbName: req.query.dbName || global.config.defaultDatabase,
        filters: filters
    }

    if (req.query.kind === undefined || req.query.kind === 'MODEL') {
        presetFilters = global.config.filters.models
    }
    if (req.query.kind === 'OBJECT') {
        presetFilters = global.config.filters.modelObjects
    }

    if (presetFilters) {
        var presetFilterValue = "";

        presetFilters.forEach(function (pf) {
            presetFilterValue = "";
            if (Array.isArray(pf.value)) {
                pf.value.forEach(function (val) {
                    presetFilterValue += val + ",";
                })
            } else {
                presetFilterValue = pf.value;
            }
            params.filters[pf.filterName] = presetFilterValue;
        });
    }
    return new Promise(function (resolve, reject) {
        databasesModel.findItemsInDatabase(params, req.session.token)
            .then(function (parsedResponse) {
                resolve(parsedResponse);
            }).catch(function (reason) {
                var statusCode = 500;
                if (reason.statusCode) statusCode = reason.statusCode;
                reject({ statusCode: statusCode, success: false, reason: reason });
        });
    });
};

exports.findItems = (req, res, callback) => {
    var chatbot = req.query.chatbot == 'true' ? true : false
    this.findItemsInDatabase(req)
        .catch(function (reason) {
            console.log("ERROR: ", reason)
            var statusCode = 500;
            if (reason.statusCode) statusCode = reason.statusCode;

            res.status(statusCode).setHeader('Content-Type', 'application/json');
            res.send({success: false, response: reason.reason});
        })
        .then(function (parsedResponse) {
            if(chatbot) {
                var items = parsedResponse.items, responseItems = [];                    
                items.forEach((i) => {
                    var model = {
                        guid: i.guid,
                        link: i.link.href,
                        apiname: i.apiname,
                        img: 'tmp/' + i.guid + '.png'
                    }
                    i.attributes.forEach((a) => {
                        if(a.apiname === "AT_CREAT_TIME_STMP") model.created = a.value;
                        else if(a.apiname === "AT_CREATOR") model.creator = a.value;
                        else if(a.apiname === "AT_NAME") model.name = a.value;
                    })
                    var params = {
                        dbName: req.query.dbName || global.config.defaultDatabase,
                        modelGUID: i.guid
                    }
                    modelsController.getGraphic(params, req.session.token)
                    responseItems.push(model)
                })
                parsedResponse = responseItems;
            }
            res.status(200).setHeader('Content-Type', 'application/json');
            res.send({success: true, databases: parsedResponse});
        });
};
