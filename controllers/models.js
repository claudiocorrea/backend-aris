var models = require('../models/models'),
    Promise = require('bluebird'),
    fs = require('fs');

exports.getGraphic = (params, token) => {    
    return new Promise(function (resolve, reject) {        
        models.getModelGraphic(params, token)
            .then(function (parsedResponse) {
                if (Array.isArray(parsedResponse.items) && parsedResponse.items.length > 0){
                    var fileName = 'tmp/' + params.modelGUID + '.png';
                    fs.writeFileSync(fileName, parsedResponse.items[0].pngdata, {encoding: 'base64'});                    
                    resolve(fs.readFileSync(fileName));
                }
            })
            .catch(function (reason) {
                var statusCode = 500;
                if (reason.statusCode) statusCode = reason.statusCode;
                reject({ statusCode: statusCode, reason: reason });
            });
    });
};

exports.findGraphic = (req, res) => {
    var params = {
        dbName: req.query.dbName || global.config.defaultDatabase,
        modelGUID: req.query.modelGUID
    }
    this.getGraphic(params, req.session.token)
        .then(function (img) {
            res.status(200).setHeader('Content-Type', 'image/png');
            res.end(img, 'binary');
        }).catch(function (reason) {
            var statusCode = 500;
            if (reason.statusCode) statusCode = reason.statusCode;
            res.status(statusCode).setHeader('Content-Type', 'application/json');
            res.send({success: false, response: reason});
    });
}