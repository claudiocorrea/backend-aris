const tokensModel = require('../models/tokens');

exports.getToken = function (req, res) {
    console.log('Creating token...');

    tokensModel.getToken({
        tenant: req.body.tenant || global.config.tenant,
        name: req.body.name || global.config.username,
        password: req.body.password || global.config.password

    }).then(function (parsedBody) {
        var tokenResponse = parsedBody.token;
        req.session.token = tokenResponse;
        req.session.user = req.body.name;
        req.session.save();

        res.status(200);
        res.setHeader('Content-Type', 'application/json');
        res.send({success: true, token: tokenResponse});

    }).catch(function (parsedBody) {
        var statusCode = 500;
        if (parsedBody.statusCode) statusCode = parsedBody.statusCode;
        res.status(statusCode);
        res.setHeader('Content-Type', 'application/json');
        res.send({success: false, response: parsedBody});
    })
};

exports.tokenValidate = (req, userData, callback) => {
    if(req.session.token) callback({ success: true, token: req.session.token });
    else tokensModel.getToken({
        tenant: userData.tenant || global.config.tenant,
        name: userData.name || global.config.username,
        password: userData.password || global.config.password

    }).then(function (parsedBody) {
        var tokenResponse = parsedBody.token;
        req.session.token = tokenResponse;
        req.session.user = userData.username;
        req.session.save();
        callback({ success: true, token: tokenResponse});

    }).catch(function (reason) {
        //console.log("- tokenValidate ERROR: ", reason);
        var err = {
            name: reason.name,
            message: reason.message,
            cause: reason.cause,
            options: reason.options
        }
        delete err.options.qs
        callback({ success: false, response: err});
    })
}