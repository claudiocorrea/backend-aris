var arisServices = module.exports = {
    "server": "http://myaris10.simbius.cloud",    
    "port": 80,

    "username": "ezequiel.ruffa",                
    "password" : "$imbius2019",                   
    "tenant": "default",                          
    "defaultDatabase" : "United Motor Group",

    "api": {
        "tokens": "/umc/api/tokens",
        "users": "/umc/api/users",
        "databases": "/abs/api/databases",
        "models": "/abs/api/models"
    },

    language: {
        default: 'en-US',
        available: []
    },

    filters: {
        models: [
            {filterName: "kind", value: "MODEL"},            
            {filterName: "attributes", value: "all"},
            //{filterName: "typefilter", value: ["MT_VAL_ADD_CHN_DGM", "MT_BPMN_COLLABORATION_DIAGRAM", "MT_EEPC_COLUMN"]}
        ],

        modelObjects: [
            {filterName: 'kind', value: 'OBJECT'},            
            {filterName: 'attributes', value: 'all'},
            //{filterName: 'typefilter', value: 'OT_FUNC'}
        ]
    },

    getURL: function () {
        return (this.port !== 80) ? this.server + ':' + this.port : this.server;
    },

    getAPI: function (api) {
        return this.api[api]
    },

    getFullAPI: function (api) {
        var apiuri = this.getAPI(api);
        if (apiuri) {
            return this.getURL() + apiuri;
        }

        return null;
    },

    getARISKey: function (encoded) {
        if (!encoded) {
            return "MIIC1DCCAbwCAwMUZTANBgkqhkiG9w0BAQsFADBTMQswCQYDVQQGEwJERTELMAkGA1UECAwCSEUxEjAQBgNVBAcMCURhcm1zdGFkdDEUMBIGA1UECgwLU29mdHdhcmUgQUcxDTALBgNVBAsMBEFSSVMwHhcNMTgwODE3MTQ0MDA3WhcNMjAwMTMxMTQ0MDA3WjCBjjELMAkGA1UEBhMCREUxFTATBgNVBAcMDFNhYXJicnVlY2tlbjEUMBIGA1UECgwLU29mdHdhcmUgQUcxDTALBgNVBAsMBEFSSVMxGzAZBgNVBAMMEnd3dy5Tb2Z0d2FyZUFHLmNvbTEmMCQGCSqGSIb3DQEJARYXbXlBUklTMTBAU29mdHdhcmVBRy5jb20wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBANpn92HHV7F5g5jUGRHFqGL2dmxN2BGV1fbjsq+PdUcd/prU1YeYj+oWssZORXVflZN7/SNuvqxP60nmf/yn170HxKN3pQ8efrmME6f2+qViCvJFdJnlFng4oD1yAMf0Ov2FCiyZ42hZC3EHfoJemmPFLSkVVZRPjteIGawF6zc1AgMBAAEwDQYJKoZIhvcNAQELBQADggEBAFqU289DpUrCj62bzBptD8Lkj+b4aEAMGBtCIML6lvrkoh0ZB579ntU2pKsNOvbuiTjWMIBPS/6b9AoX+TQvWW1edZveBdvW8RHr0n2d6HQVvuDmWbZMotmCrrNIup4dtQikM1LprXI+OMTp5w7BSdgnEUseirxqB6kBLgxNbdIZ2wJehCAgq+iTuSc+irI6PLfNT4IK7qoqcMaG04t3xdVhCEILKscpLaheBfKOYF4R5hwe9fPVIDSY188qD3+YshMp2FF6Qj1R+Ag8Ock7zOppFF05pCLBvrhsiKBFalgSrhW1PvTUsgj9r4iB4Ju5Vcn610yQzIHiLojVkgR6YTw=";           
        } else {
            return encodeURIComponent("MIIC1DCCAbwCAwMUZTANBgkqhkiG9w0BAQsFADBTMQswCQYDVQQGEwJERTELMAkGA1UECAwCSEUxEjAQBgNVBAcMCURhcm1zdGFkdDEUMBIGA1UECgwLU29mdHdhcmUgQUcxDTALBgNVBAsMBEFSSVMwHhcNMTgwODE3MTQ0MDA3WhcNMjAwMTMxMTQ0MDA3WjCBjjELMAkGA1UEBhMCREUxFTATBgNVBAcMDFNhYXJicnVlY2tlbjEUMBIGA1UECgwLU29mdHdhcmUgQUcxDTALBgNVBAsMBEFSSVMxGzAZBgNVBAMMEnd3dy5Tb2Z0d2FyZUFHLmNvbTEmMCQGCSqGSIb3DQEJARYXbXlBUklTMTBAU29mdHdhcmVBRy5jb20wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBANpn92HHV7F5g5jUGRHFqGL2dmxN2BGV1fbjsq+PdUcd/prU1YeYj+oWssZORXVflZN7/SNuvqxP60nmf/yn170HxKN3pQ8efrmME6f2+qViCvJFdJnlFng4oD1yAMf0Ov2FCiyZ42hZC3EHfoJemmPFLSkVVZRPjteIGawF6zc1AgMBAAEwDQYJKoZIhvcNAQELBQADggEBAFqU289DpUrCj62bzBptD8Lkj+b4aEAMGBtCIML6lvrkoh0ZB579ntU2pKsNOvbuiTjWMIBPS/6b9AoX+TQvWW1edZveBdvW8RHr0n2d6HQVvuDmWbZMotmCrrNIup4dtQikM1LprXI+OMTp5w7BSdgnEUseirxqB6kBLgxNbdIZ2wJehCAgq+iTuSc+irI6PLfNT4IK7qoqcMaG04t3xdVhCEILKscpLaheBfKOYF4R5hwe9fPVIDSY188qD3+YshMp2FF6Qj1R+Ag8Ock7zOppFF05pCLBvrhsiKBFalgSrhW1PvTUsgj9r4iB4Ju5Vcn610yQzIHiLojVkgR6YTw=");
        }
    }
}
