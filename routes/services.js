const express = require('express'),
    servicesRouter = express.Router(),
    tokensController = require('../controllers/tokens'),
    databasesController = require('../controllers/databases')
    modelsController = require('../controllers/models')
    ;

module.exports = servicesRouter;

function tokenValidate(req, res, next) {
    if (req.session.token) { return next(); } 
    else {
        tokensController.tokenValidate(req, {}, (validateResponse) => {
            if(!validateResponse.success) {
                var statusCode = 301;
                if (validateResponse.statusCode) statusCode = validateResponse.statusCode;
                res.status(statusCode).setHeader('Content-Type', 'application/json');
                res.send({success: false, response: validateResponse.response});
            } else return next();
        })
    }
}
  
servicesRouter.get('/databases', tokenValidate, databasesController.getDataBases);
servicesRouter.get('/databases/find', tokenValidate, databasesController.findItems);

servicesRouter.get('/models/graphic', tokenValidate, modelsController.findGraphic);


// FOR CHATBOT
//servicesRouter.get('/databases/findByChatbot', databasesController.findItemsByChatbot);
//servicesRouter.get('/databases/getAllProcess', databasesController.getAllProcess);




